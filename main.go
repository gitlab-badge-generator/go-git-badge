package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/awslabs/aws-lambda-go-api-proxy/gin"
	"github.com/gin-gonic/gin"
	"github.com/narqo/go-badge"
	"github.com/thedevsaddam/gojsonq"
)

var initialized = false
var ginLambda *ginadapter.GinLambda
var gitlab string
var private string
var colorMap = map[string]string{
	"pending":  "#ffffff",
	"running":  "#ffff00",
	"passed":   "#00ff00",
	"failed":   "#cc0000",
	"skipped":  "#0000ff",
	"canceled": "#000000",
	"unknown":  "#aaaaaa",
}

func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	if !initialized {
		// stdout and stderr are sent to AWS CloudWatch Logs
		log.Printf("Gin cold start")
		router := gin.Default()

		router.GET("/", func(c *gin.Context) {
			c.Header("server", "my-server")
			c.Header("set-cookie", "name=value")
			c.Set("statusDescription", "HTTP OK")
			c.Set("isBase64Encoded", "False")
			c.String(http.StatusOK, "healthy")
		})

		router.GET("/:project/:environment/*action", func(c *gin.Context) {
			// Get Parameters
			project := c.Param("project")
			environment := c.Param("environment")
			action := c.Param("action")
			log.Println(project)
			log.Println(environment)
			log.Println(action)

			// Get Job Info
			projectid, err := strconv.Atoi(project)
			checkError(err)
			deploymentBody := getLastJob(projectid)

			// Check if svg or url
			if action != "/badge.svg" {
				// Redirect
				redirect := getJobInfo(deploymentBody, environment, "web_url")
				c.Redirect(http.StatusTemporaryRedirect, redirect)
			} else {
				// Badge
				status := getJobInfo(deploymentBody, environment, "status")
				pipelineColor := colorMap[status]
				var b bytes.Buffer
				bufio.NewWriter(&b)
				checkError(badge.Render(environment, status, badge.Color(pipelineColor), io.Writer(&b)))
				c.Data(http.StatusOK, "image/svg+xml", b.Bytes())
			}
		})

		// Lambda Provider
		ginLambda = ginadapter.New(router)
		initialized = true
	}

	// If no name is provided in the HTTP request body, throw an error
	return ginLambda.Proxy(req)
}

func main() {
	if checkEnvironment() {
		lambda.Start(Handler)
	} else {
		lambda.Start(Handler)
		// log.Println("Check you environment variables")
	}
}

func checkEnvironment() bool {
	gitlab = os.Getenv("GITLABURL")
	if len(gitlab) == 0 {
		return false
	}
	private = os.Getenv("PRIVATETOKEN")
	if len(private) == 0 {
		return false
	}
	return true
}

func getLastJob(project int) string {
	client := &http.Client{}
	templateURL := "https://%s/api/v4/projects/%d/deployments?sort=desc"
	reqURL := fmt.Sprintf(templateURL, gitlab, project)
	req, err := http.NewRequest("GET", reqURL, nil)
	checkError(err)
	req.Header.Add("PRIVATE-TOKEN", private)
	resp, err := client.Do(req)
	checkError(err)
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		checkError(err)
		bodyString := string(bodyBytes)
		return bodyString
	}
	return ""
}

func getJobInfo(body string, environment string, pipeline string) string {
	// Dirty Hack cause shitty library
	jq := gojsonq.New().JSONString(body)
	name := "something"
	i := 0
	templateSearch := "[%d].environment.name"
	templateJob := "[%d].deployable.pipeline.%s"
	for name != "" { // TODO: Need Interface Type Checking
		name = (jq.Reset().Find(fmt.Sprintf(templateSearch, i))).(string)
		if name == environment {
			job := (jq.Reset().Find(fmt.Sprintf(templateJob, i, pipeline))).(string)
			return job
		}
		i++
	}
	return name
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
